#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ShotusGameMode.generated.h"

UCLASS(minimalapi)
class AShotusGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AShotusGameMode();
};




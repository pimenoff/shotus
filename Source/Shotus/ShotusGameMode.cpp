#include "ShotusGameMode.h"
#include "ShotusCharacter.h"
#include "UObject/ConstructorHelpers.h"

AShotusGameMode::AShotusGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Shotus/Blueprints/BP_ShotusCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
